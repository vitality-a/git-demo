# git



设置用户签名

```
git config --global user.name yuanqi
```

设置用户邮箱

```
git config --global user.email 12345@qq.com
```

![image-20211119192535752](C:\Users\21115\AppData\Roaming\Typora\typora-user-images\image-20211119192535752.png)

初始化

```
git init
```



添加到暂存区

```
git add 123.txt
```



提交

```
git commit 123.txt
```



查看当前状态

```
git status
```



查看所有版本更替（精简）

```
git reflog
```



查看所有版本更替（详细）

```
git log
```



版本穿梭

```
git reset --hard 124e9c6
```

![image-20211119192935393](C:\Users\21115\AppData\Roaming\Typora\typora-user-images\image-20211119192935393.png)



查看所有分支

```
git branch -v
git branch
```



创建分支

```
git branch newBranch
```

![image-20211119194941761](C:\Users\21115\AppData\Roaming\Typora\typora-user-images\image-20211119194941761.png)



切换分支

```
git checkout newBranch
```

![image-20211119195054497](C:\Users\21115\AppData\Roaming\Typora\typora-user-images\image-20211119195054497.png)



合并指定分支到当前分支下

```
git merge newBranch
```

![image-20211119195440467](C:\Users\21115\AppData\Roaming\Typora\typora-user-images\image-20211119195440467.png)



查看远程库

```
git remote -v
```



创建远程库

```
git remote add 别名 URL

eg:
git remote add git-demo https://github.com/ggg2111/git-demo.git
```

![image-20211119201841319](C:\Users\21115\AppData\Roaming\Typora\typora-user-images\image-20211119201841319.png)



推送本地到远程仓库

```
git push 别名 本地分支名

eg:
git push git-demo master
```



拉取远程到本地仓库

```
git pull 别名 远程分支名

eg:
git pull git-demo master
```



克隆远程项目到本地

```
git clone https://gitee.com/vitality-a/git-demo.git
```



